---
created: 2021-06-26T15:50:46+02:00
title: Logging to registries with Toolbox
draft: false
tags: [fedora, toolbox, linux, silver blue, testing]
modified: 2021-06-26T16:45:49+02:00
---

In version [0.0.99](https://github.com/containers/toolbox/releases/tag/0.0.99) of Toolbox we added support for UBI, Universal Base Image, which serve for getting a "Toolbox-ready" environment. While the feature will be a subject of several changes including a change of the used image (which will, of course, remain being UBI-based), the potential use on RHEL (where we are trying to get Toolbox) showed one major shortcoming of Toolbox: no support for registries with authentication.

I spent some time working on this feature and managed to [put it together](https://github.com/containers/toolbox/pull/787).

## What's new

If you try to pull an image from e.g. Docker Hub and the image either does not exist or is private, Toolbox will recognise the error returned by the registry and ask you if you want to login into the registry. If you agree, a prompt, coming from Podman, is offered to enter you user credentials. On success the image pull is retried.

This feature comes with a pair of new options:

- `--authfile` - file with saved login credentials; managed by Podman
- `--creds` - option for passing login information (username + password)

## How does it look?

```shell
# Try to create from an unknown image
$ toolbox create -i registry.fedoraproject.org/no-image
Image required to create toolbox container.
Download registry.fedoraproject.org/no-image (500MB)? [y/N]: y
Error: The requested image does not exist
Make sure the image URI is correct.

# Try to create from an image in registry with authentication
$ toolbox -y create -i registry.redhat.io/image
Image required to create toolbox container.
Download registry.redhat.io/image (500MB)? [y/N]: y
The registry requires logging in.
Do you want to log into the registry and try to pull the image again? [y/N]: y
Username: <username>
Password:
Retrying to pull image registry.redhat.io/image
Created container: image-toolbox
Enter with: toolbox enter image-toolbox

# Create from an image in registry with authentication by providing login credentials
$ toolbox -y create -i registry.redhat.io/image --creds <username>:<password>
Could not pull image registry.redhat.io/image
The registry requires logging in.
Credentials were provided. Trying to log into registry.redhat.io
Login Succeeded!
Retrying to pull image registry.redhat.io/image
Created container: image-toolbox
Enter with: toolbox enter image-toolbox
```

# How is it tested?

The feature required a non-trivial adjustment of our system tests since the functionality mainly relies on resources available over the internet network which is inherently unreliable.

Before the test suite is started, two locally hosted [Docker registries](https://hub.docker.com/_/registry) are hosted in a separate Podman root (option `--root`). For each of those a locally signed certificate is created and made known to Podman by putting it into its certificate directory (e.g., `~/.config/containers/certs.d`). And for one of the registries a `htpasswd` file is created containing a definition of a single rest user. The other registry does not have -> no authentication.

## Anything left to be done?

Currently the code is tested only against the Docker registry but I'd like to test it against Quay.io and whatever powers [registry.redhat.io](https://registry.redhat.io) and [registry.access.redhat.com](https://registry.access.redhat.com) if it is not Quay. Also, [registry.fedoraproject.org](https://registry.fedoraproject.org) is a registry I'd like to test again. If you know what powers these registries and how I could set up those locally, let me know! Thank you!
